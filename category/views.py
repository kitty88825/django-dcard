from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from .models import Category
from .serializer import CategorySerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [AllowAny]
